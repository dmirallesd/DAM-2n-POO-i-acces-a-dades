// Qüestionari corresponent als apartats de diagrames de classes, classes i interfícies, característiques, i generalitzacions.

::Definició diagrama de classes::Els diagrames de classes representen... {
  =relacions estàtiques entre els tipus d'objecte d'un sistema.
  ~relacions d'example entre els objectes d'un sistema. #Això ho fa un diagrama d'objectes.
  ~com els usuaris interactuen amb un sistema. #Això ho fa un diagrama de casos d'ús.
  ~una seqüència d'interaccions entre objectes. #Això ho fa un diagrama de seqüència.
}

::Diagrama E-R i diagrama de classes::[markdown]
Quina de les següents **NO** és una diferència entre un diagrama E-R i un diagrama de classes? {
  ~Els diagrames E-R modelen només dades, però els diagrames de classes també modelen operacions.
  ~Els diagrames E-R no tenen suport per modelar conceptes com el d'interfície, però el diagrama de classes sí.
  ~Els diagrames de classes poden mostrar relacions entre classes que no existeixen en un diagrama E-R.
  =Els diagrames de classes poden modelar les propietats d'una classe i la seva multiplicitat, i els diagrames E-R no.
}

::Utilitat diagrama de classes::[markdown]
Què **NO** és cert sobre els diagrames de classes? {
  ~Indicar la multiplicitat, visibilitat, i fins i tot les característiques d'una classe, és opcional.
  =Habitualment, l'objectiu d'un diagrama de classes és representar la totalitat de classes i relacions que hi ha en un sistema.
  ~En els diagrames de classes, a més de classes, també es poden representar interfícies i classes abstractes.
  ~Les propietats que es representen en un diagrama de classes no sempre s'han d'implementar exactament com apareixen al diagrama.
}

::Propietats d'una classe::Les propietats d'una classe... {
  =Es poden representar com a atributs o com associacions.
  ~Es mostren sempre com atributs de la classe.
  ~Es mostren sempre com associacions entre tipus d'objecte.
  ~Es poden representar com a atributs i com operacions de la classe.
}

::Implementació de les propietats::Què és cert pel que fa a les propietats? {
  ~%-33.33333%Una propietat sempre es correspon amb un atribut de la classe.
  ~%33.33333%Una propietat public es pot implementar com un atribut private i els seus mètodes get i set.
  ~%33.33333%Una propietat podria no implementar-se com un atribut en alguns casos.
  ~%33.33333%Una propietat multivaluada pot representar un array o una llista sense que això s'indiqui explícitament al diagrama.
}

::Notació com associacions i com a atributs::En l'exemple del personatge, el seu inventari i mascotes (Character, Inventory, Pet), què podem saber si utilitzem la notació en forma d'associació, que no podem deduir si utilitzem la d'atributs? {
  ~%33.33333%Que una mascota sempre ha de pertànyer a un jugador i només a un jugador.
  ~%33.33333%Que no hi pot haver dos personatges amb el mateix nom.
  ~%33.33333%Que dos jugadors no poden posseir el mateix objecte, però pot passar que un objecte no estigui a l'inventari de cap jugador.
  ~%-33.33333%Que un jugador pot tenir una mascota o cap, però mai dues o més mascotes.
  ~%-33.33333%Que un jugador pot tenir múltiples ítems al seu inventari, i fins i tot pot no tenir-ne cap.
}

::Aplicació de l'herència::Quin és el principi més important a l'hora de decidir si aplicar l'herència és una bona idea o no? {
  =El principi de substituibilitat, que diu que s'ha de poder passar un objecte del subtipus a qualsevol operació que esperi un objecte del supertipus.
  ~El principi de substituibilitat, que diu que s'ha de poder passar un objecte del supertipus a qualsevol operació que esperi un objecte del subtipus.
  ~S'ha de poder dir que un objecte del subtipus és un objecte del supertipus.
  ~El subtipus i el supertipus han de compartir una part important del codi, i el subtipus ha de tenir algunes característiques pròpies.
}

::Generalització i classificació::Què hem après de l'exemple dels gossos i els pastors alemanys? {
  ~Que els pastors alemanys constitueixen una espècie.
  ~Que la relació d'herència no és transitiva: si A deriva de B, i B deriva de C, no té per què ser cert que A derivi de C.
  =Que l'herència no és sempre una forma adequada de representar una relació de tipus "A és un B".
  ~Que l'herència és un mecanisme de la programació orientada a objectes que cal evitar utilitzar.
}
