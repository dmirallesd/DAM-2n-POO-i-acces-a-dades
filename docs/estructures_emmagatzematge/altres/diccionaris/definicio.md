##### Definició d'un diccionari

En Java, la interfície **Map** defineix les operacions comunes a totes les
implementacions dels diccionaris. **SortedMap** estén *Map* i manté les
claus que s'hi introdueixen ordenades. **NavigableMap** estén *Map* i ofereix
mètodes que permeten localitzar les claus més properes a un valor donat.

*Map* no hereta de *Collection*, així que no tenim les operacions
habituals de les altres col·leccions, en particular, no tenim iteradors
per recórrer directament tot el contingut del diccionari.

Les dues implementacions principals de *Map* són **HashMap** i **TreeMap**. El
*HashMap* no garanteix l'ordre dels seus elements (és a dir, no és un
*SortedMap*), però és molt eficient i els temps d'inserció i supressió
d'elements és constant. El *TreeMap* sí que manté els seus elements
ordenats, però és menys eficient que el *HashMap*.

Per **definir un diccionari**:

 * Per definir un diccionari cal especificar de quin tipus seran les claus
i de quin tipus els valors. Per exemple:
`Map<String, Integer> d = new HashMap<String, Integer>(); `serà
un diccionari que tindrà cadenes com a claus i enters com a valors.

 * Per afegir elements:

 ```java
 d.put("Altura", 10);
 d.put("Amplada", 5);
 d.put("Fondària", 3);
 ```

 O si tenim els elements en dos vectors, podríem fer:

 ```java
 for (i=0; i<clausInicials.length; i++)
     d.put(clausInicials[i], valorsInicials[i]);
 ```

 * A partir d'un diccionari, crear-ne un altre independent (i possiblement
   de tipus diferent):

 `SortedMap<String, Integer> d2 = new TreeMap<String, Integer>(d);`

 Això crea un diccionari amb els mateixos valors que el primer
 diccionari, però que és independent, en el sentit que si modifiquem el
 diccionari original no es modifica la còpia.
