### Els pilars de la programació orientada a objectes

 * [Encapsulació](encapsulacio.md)
 * [Abstracció](abstraccio.md)
 * [Herència](herencia.md)
 * [Composició](composicio.md)
 * [Polimorfisme](polimorfisme.md)
 
