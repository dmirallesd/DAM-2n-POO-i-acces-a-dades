## Exercicis

### Format JSON

#### Exercici 1

Tradueix el següent fragment XML (extret de
http://www.w3schools.com/xml/cd_catalog.xml) a JSON.

```xml
<CATALOG>
  <CD>
    <TITLE>Empire Burlesque</TITLE>
    <ARTIST>Bob Dylan</ARTIST>
    <COUNTRY>USA</COUNTRY>
    <COMPANY>Columbia</COMPANY>
    <PRICE>10.90</PRICE>
    <YEAR>1985</YEAR>
  </CD>
  <CD>
    <TITLE>Hide your heart</TITLE>
    <ARTIST>Bonnie Tyler</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>CBS Records</COMPANY>
    <PRICE>9.90</PRICE>
    <YEAR>1988</YEAR>
  </CD>
</CATALOG>
```

#### Exercici 2

Tradueix el següent fragment XML (extret de http://json.org/example.html) a
JSON.

```xml
<menu id="file" value="File">
  <popup>
    <menuitem value="New" onclick="CreateNewDoc()" />
    <menuitem value="Open" onclick="OpenDoc()" />
    <menuitem value="Close" onclick="CloseDoc()" />
  </popup>
</menu>
```

#### Exercici 3

Tradueix el següent fragment JSON (extret de http://json.org/example.html)
a XML.

```json
{
  "glossary": {
    "title": "example glossary",
		"GlossDiv": {
      "title": "S",
			"GlossList": {
        "GlossEntry": {
          "ID": "SGML",
					"SortAs": "SGML",
					"GlossTerm": "Standard Generalized Markup Language",
					"Acronym": "SGML",
					"Abbrev": "ISO 8879:1986",
					"GlossDef": {
            "para": "A meta-markup language, used to create markup languages such as DocBook.",
						"GlossSeeAlso": ["GML", "XML"]
          },
					"GlossSee": "markup"
        }
      }
    }
  }
}
```

[Solucions](solucions_format_json.md)

### Representació de documents

#### Exercici 1

Utilitza les classes proporcionades pel connector de MongoDB per crear un
programa en Java que creï el següent document JSON.

Utilitza un bucle per afegir els actors i no repetir codi.

```json
{
	"_id" : 19,
	"Title" : "AMADEUS HOLY",
	"Description" : "A Emotional Display of a Pioneer And a Technical Writer who must Battle a Man in A Baloon",
	"Length" : 113,
	"Rating" : "PG",
	"Special Features" : "Commentaries,Deleted Scenes,Behind the Scenes",
	"Rental Duration" : 6,
	"Replacement Cost" : 20.99,
	"Category" : "Action",
	"Actors" : [
		{
			"actorId" : 5,
			"Last name" : "LOLLOBRIGIDA",
			"First name" : "JOHNNY"
		},
		{
			"actorId" : 27,
			"Last name" : "MCQUEEN",
			"First name" : "JULIA"
		},
		{
			"actorId" : 37,
			"Last name" : "BOLGER",
			"First name" : "VAL"
		},
		{
			"actorId" : 43,
			"Last name" : "JOVOVICH",
			"First name" : "KIRK"
		},
		{
			"actorId" : 84,
			"Last name" : "PITT",
			"First name" : "JAMES"
		},
		{
			"actorId" : 104,
			"Last name" : "CRONYN",
			"First name" : "PENELOPE"
		}
	]
}

```

#### Exercici 2

Fes el mateix amb el següent document.

Pots considerar que les dates es guarden en format de cadena.

Utilitza un bucle per no repetir codi a l'hora d'afegir els *Rentals*.

```json
{
	"_id" : 1,
	"First Name" : "MARY",
	"Last Name" : "SMITH",
	"Address" : "1913 Hanoi Way",
	"District" : "Nagasaki",
	"City" : "Sasebo",
	"Country" : "Japan",
	"Phone" : "28303384290",
	"Rentals" : [
		{
			"rentalId" : 1185,
			"Rental Date" : "2005-06-15 00:54:12.0",
			"Return Date" : "2005-06-23 02:42:12.0",
			"staffId" : 2,
			"filmId" : 611,
			"Film Title" : "MUSKETEERS WAIT",
			"Payments" : [
				{
					"Payment Id" : 3,
					"Amount" : 6.00,
					"Payment Date" : "2005-06-15 00:54:12.0"
				}
			]
		},
		{
			"rentalId" : 1476,
			"Rental Date" : "2005-06-15 21:08:46.0",
			"Return Date" : "2005-06-25 02:26:46.0",
			"staffId" : 1,
			"filmId" : 308,
			"Film Title" : "FERRIS MOTHER",
			"Payments" : [
				{
					"Payment Id" : 5,
					"Amount" : 10.00,
					"Payment Date" : "2005-06-15 21:08:46.0"
				}
			]
		},
		{
			"rentalId" : 1725,
			"Rental Date" : "2005-06-16 15:18:57.0",
			"Return Date" : "2005-06-17 21:05:57.0",
			"staffId" : 1,
			"filmId" : 159,
			"Film Title" : "CLOSER BANG",
			"Payments" : [
				{
					"Payment Id" : 6,
					"Amount" : 5.00,
					"Payment Date" : "2005-06-16 15:18:57.0"
				}
			]
		}
  ]
}
```
