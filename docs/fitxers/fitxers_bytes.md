## Lectura i escriptura de fitxers byte a byte

Habitualment no treballarem mai amb un fitxer byte a byte, però els altres
accessos a fitxers que farem treballen internament utilitzant les classes
que veurem en aquest apartat.

Tots els fluxos de dades (*streams*) treballen a partir de les classes
*InputStream* i *OutputStream*. Aquestes dues classes són abstractes i les
API ens proporcionen una colla de classes derivades especialitzades en la
lectura i escriptura de diversos tipus de fluxos de dades.

Per a la lectura i escriptura de fitxers s'utilitzen les classes
*FileInputStream* i *FileOutputStream*.

### Escriptura d'un fitxer byte a byte

En aquest exemple escrivim una colla de bytes en un fitxer:

```java
public class EscriptorBytes {

	public static void main(String[] args) {
		/* Creant l'objecte en el try fem que es cridi al seu mètode close() automàticament
		 * quan el try acaba, encara que salti una excepció. Això es pot fer amb qualsevol
		 * classe que implementi Closeable.
		 *
		 * Si no fem així, hauríem d'afegir una clàusula finally després dels catch i
		 * fer escriptor.close() allà.
		 */
		try(FileOutputStream escriptor = new FileOutputStream("prova.txt")) {
			for (char ch : "hola".toCharArray()) {
				escriptor.write(ch);
			}
			for (byte b=65; b<80; b++) {
				escriptor.write(b);
			}
      byte[] bytes = {97, 98, 99, 100, 101, 102, 103, 104, 105};
			escriptor.write(bytes, 2, 5); // escriu 5 bytes començant per la posició 2.
		} catch (FileNotFoundException e) {
			// No vol dir que no existeixi, si no que no hi pot escriure.
			System.err.println(e);
		} catch (IOException e) {
			// Podríem haver posat només aquesta excepció, perquè FileNotFounException
			// deriva de IOException.
			System.err.println(e);
		}
	}
}
```

Si obrim el fitxer *prova.txt* després de l'execució d'aquest programa
trobarem el text "*holaABCDEFGHIJKLMNOcdefg*".

Això funciona perquè aquests caràcters es codifiquen en UTF-8 utilitzant un
sol byte, que coincideix amb el codi ASCII. Però no funcionaria amb altres
caràcters i símbols, que ocupen més d'un byte (un char en Java ocupa 2 bytes).

És a dir, **aquesta no és la forma correcta d'escriure text en un fitxer**.

### Lectura d'un fitxer byte a byte

En el següent exemple llegim el fitxer que hem creat a l'apartat anterior.

El llegirem dues vegades: a la primera el llegirem byte a byte, mentre que a
la segona el llegirem sencer i guardarem els bytes llegits a un array.

```java
public class LectorBytes {

	public static void main(String[] args) {
		int b=0;

		try (FileInputStream lector = new FileInputStream("prova.txt")) {
			// Llegeix byte a byte. -1 indica final de fitxer.
			while (b!=-1) {
				b = lector.read();
				if (b!=-1)
					System.out.print(b+" ");
			}
			System.out.println();
		} catch (FileNotFoundException e) {
			System.err.println(e);
		} catch (IOException e) {
			System.err.println(e);
		}

		byte[] contingut = new byte[100];
		int llegits;
		try (FileInputStream lector = new FileInputStream("prova.txt")) {
			// Llegeix tot el fitxer i ho guarda en un array.
			llegits = lector.read(contingut);
			if (llegits==-1) {
				System.out.println("S'ha arribat a final de fitxer.");
			} else {
				System.out.println("S'han llegit "+llegits+" bytes.");
			}
		} catch (FileNotFoundException e) {
			System.err.println(e);
		} catch (IOException e) {
			System.err.println(e);
		}
	}
}
```

Si executem aquest programa sobre el fitxer creat abans el resultat és el
següent:

```
104 111 108 97 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 99 100 101 102 103
S'han llegit 24 bytes.
```
