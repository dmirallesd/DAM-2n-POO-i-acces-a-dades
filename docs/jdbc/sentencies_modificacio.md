## Sentències DML. Modificació de dades

```java
public class CrearInsert {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/employees";
		Properties propietatsConnexio = new Properties();
		propietatsConnexio.put("user", "root");
		propietatsConnexio.put("password", "usbw");

		try (Connection con = DriverManager.getConnection(url,propietatsConnexio);
				Statement st = con.createStatement()) {
			int numFiles= st.executeUpdate("INSERT INTO "
					+ "employees(birth_date, first_name, last_name, gender, hire_date) "
					+ "VALUES('1953-03-16','Richard','Stallman','M','1975-01-21')");						
			System.out.println("Inserció creada! Files afectades: " + numFiles);
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}
	}
}
```

```java
public class InsercioIdiomes {
	public static void main(String[] args) {
		if (args.length == 1) {
			try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost/sakila", "root",
					"usbw");
					PreparedStatement comptaIdioma = connection
							.prepareStatement("SELECT count(*) FROM language WHERE name=?");
					PreparedStatement insereixIdioma = connection
							.prepareStatement("INSERT INTO language(name) VALUES (?)");) {
				comptaIdioma.setString(1, args[0]);
				try (ResultSet rs = comptaIdioma.executeQuery()) {
					if (rs.next() && rs.getInt(1) == 0) {
						insereixIdioma.setString(1, args[0]);
						/*
						 * executeUpdate permet executar sentències DML (insert,
						 * update, delete) i DDL (create, drop, alter). Per DML,
						 * retorna número de files afectades.
						 */
						int files = insereixIdioma.executeUpdate();
						if (files == 1) {
							System.out.println("S'ha insertat la fila correctament");
						} else {
							System.err.println("No s'ha pogut insertar la fila");
						}
					} else {
						System.err.println("Aquest idioma ja és a la llista");
					}
				}
			} catch (SQLException e) {
				/*
				 * Una excepció SQLException permet obtenir molta informació
				 * sobre l'error que s'ha produït:
				 */
				// Missatge retornat pel SGBD
				System.err.println("Missatge: " + e.getMessage());
				// Codi d'error estàndard
				System.err.println("Estat SQL: " + e.getSQLState());
				// Codi d'error propi del fabricant del SGBD
				System.err.println("Codi de l'error: " + e.getErrorCode());
			}
		} else {
			System.err.println("S'esperava el nom d'un idioma");
		}
	}

}
```
