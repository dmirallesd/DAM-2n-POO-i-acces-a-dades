package exemple_validador_xml;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

public class ValidadorDOM {
	public static void main(String args[]) {
		valida("alumnes.xml");
	}

	public static boolean valida(String rutafitxerXML) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(true);
			factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			builder.setErrorHandler(new ControladorErrades());
			builder.parse(new InputSource(rutafitxerXML));
			return true;
		} catch (ParserConfigurationException | IOException | SAXException e) {
			System.err.println(e.getMessage());
			return false;
		}
	}
}
