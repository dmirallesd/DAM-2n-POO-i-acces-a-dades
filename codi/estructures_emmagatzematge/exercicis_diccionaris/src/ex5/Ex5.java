package ex5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Ex5 {

	// TODO: millorar per què passi a majúscula el principi de cada paraula.
	// Veure com es fa servir un BreakIterator.
	public static String capitalize(String s) {
		if (s.length() > 0)
			return Character.toUpperCase(s.charAt(0))+s.substring(1);
		else
			return "";
	}
	
	public static void main(String[] args) {
		Map<String, String> paisos = new HashMap<String, String>();
		BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));
		String pais = " ";
		String capital;
		
		paisos.put("Espanya", "Madrid");
		paisos.put("França", "París");
		paisos.put("Itàlia", "Roma");
		paisos.put("Anglaterra", "Londres");
		paisos.put("Alemanya", "Berlín");
		
		try {
			while (!pais.equals("")) {
				System.out.print("Introdueix un nom de país: ");
				pais = entrada.readLine().toLowerCase();
				if (!pais.equals("")) {
					pais = capitalize(pais);
					capital = paisos.get(pais);
					if (capital != null) {
						System.out.println("La capital de "+pais+" és "+capital);
					} else {
						System.out.print("Quina és la capital de "+pais+"?");
						capital = entrada.readLine();
						if (!capital.equals(""))
							paisos.put(pais, capitalize(capital));
					}
				}
			}
		} catch (IOException e) {
			System.out.println("Error en l'entrada");
		}

	}

}
