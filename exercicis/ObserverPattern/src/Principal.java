
public class Principal {

	public static void main(String[] args) {
		
		GuardaEsdeveniments ge = new GuardaEsdeveniments();
		MostraEsdeveniments me = new MostraEsdeveniments();
		ProdueixEsdeveniments pe = new ProdueixEsdeveniments();
		
		pe.addEventListener(ge);
		pe.addEventListener(me);
		
		for(int i=0; i<5; i++)
			pe.creaEsdeveniment(i);
		
		System.out.println(ge.toString());
	}

}
