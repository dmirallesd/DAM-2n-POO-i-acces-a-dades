import java.util.ArrayList;
import java.util.List;

public class GuardaEsdeveniments implements Listener{

	private List<Integer> llistaEsdeveniments = new ArrayList<Integer>();
	
	public List<Integer> getLlista(){
		return this.llistaEsdeveniments;
	}
	
	@Override
	public void notifyEvent(int event) {
		llistaEsdeveniments.add(event);
	}
	
	@Override
	public String toString(){
		String res = "";

		for(Integer event : llistaEsdeveniments)
			res += "_" + event.toString();
		
		return res;
	}
	
}
