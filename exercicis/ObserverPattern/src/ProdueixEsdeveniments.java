import java.util.ArrayList;
import java.util.List;

public class ProdueixEsdeveniments {

	private List<Listener> llista = new ArrayList<Listener>();
	
	public List<Listener> getLlista(){
		return this.llista;
	}
	
	public void addEventListener(Listener l){
		llista.add(l);
	}
	
	public void creaEsdeveniment(int event){
		for(Listener li : llista)
			li.notifyEvent(event);
	}
}
